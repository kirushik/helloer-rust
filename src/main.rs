extern crate iron;
extern crate staticfile;
extern crate mount;

use std::path::Path;

use iron::Iron;
use staticfile::Static;
use mount::Mount;

fn main() {
    let folder = std::env::args().nth(1).unwrap_or(".".into());
    println!("Serving files from {}", folder);

    let mut mount = Mount::new();
    mount.mount("/", Static::new(Path::new(&folder)));
    Iron::new(mount).http("127.0.0.1:3000").unwrap();
}
